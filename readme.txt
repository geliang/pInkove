包含字符串 int long char* int[] struct 函数指针 的传递  多线程异步回调




C++通过callback向C#传递数据必须注意以下几点：

1、C++中的回调函数必须用_stdcall标记，使用stdcall方式回调；

2、如果是数组，必须用 [MarshalAs(UnmanagedType.LPArray, SizeConst = 23)]标记参数，指定为数组且标记数组长度；

3、C#方必须申明一个变量，用来指向C++的回调指针函数，避免被C#回收掉。