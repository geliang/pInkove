副标题: P/Invoke，C++ Interop和COM Interop

作者: 黄际洲 / 崔晓源

出版社: 人民邮电出版社

出版年: 2009-5

页数: 419

定价: 68.00元

装帧: 平装

ISBN: 9787115204349


内容简介 · · · · · · 

　　《精通.NET互操作P/Invoke,C++Interop和COM Interop》介绍Windows平台上的托管代码与非托管代码之间进行互操作的各种技术，包括由.NET提供的各种互操作方法、属性以及各种工具的用法及其工作原理。《精通.NET互操作P/Invoke,C++Interop和COM Interop》包括3部分，平台调用——主要用于解决在托管代码中调用非托管程序设计语言编写的flat API(如Win32 API、C/C++风格的API等)的问题；C++ Interop——技术专门用于解决托管代码与C++编写的非托管代码之间的互操作问题；COM Interop——介绍了使用COM Interop解决在托管代码中调用COM组件，以及在COM中调用托管类型的问题。《精通.NET互操作P/Invoke,C++Interop和COM Interop... (展开全部) 　　《精通.NET互操作P/Invoke,C++Interop和COM Interop》介绍Windows平台上的托管代码与非托管代码之间进行互操作的各种技术，包括由.NET提供的各种互操作方法、属性以及各种工具的用法及其工作原理。《精通.NET互操作P/Invoke,C++Interop和COM Interop》包括3部分，平台调用——主要用于解决在托管代码中调用非托管程序设计语言编写的flat API(如Win32 API、C/C++风格的API等)的问题；C++ Interop——技术专门用于解决托管代码与C++编写的非托管代码之间的互操作问题；COM Interop——介绍了使用COM Interop解决在托管代码中调用COM组件，以及在COM中调用托管类型的问题。《精通.NET互操作P/Invoke,C++Interop和COM Interop》适合所有在开发过程中需要涉及到托管代码与非托管代码进行交互操作的.NET开发人员阅读使用。不论是开始学习.NET编程的开发人员，还是刚刚接触互操作的资深.NET开发人员，都能从《精通.NET互操作P/Invoke,C++Interop和COM Interop》中获益。　　

