﻿namespace MatchVS {
    public partial class NativeConstants {

        /// __MATCHVS_H__ -> 
        /// Error generating expression: 值不能为空。
        ///参数名: node
        public const string @__MATCHVS_H__ = "";

        /// DLLAPI -> __declspec(dllexport)
        /// Error generating expression: Error generating function call.  Operation not implemented
        public const string DLLAPI = "__declspec(dllexport)";

        /// FunStackPushType -> __stdcall
        /// Error generating expression: Value __stdcall is not resolved
        public const string FunStackPushType = "__stdcall";

        /// MATCHVS_DEALING -> 0
        public const int MATCHVS_DEALING = 0;

        /// MATCHVS_FAILED -> 1
        public const int MATCHVS_FAILED = 1;

        /// MATCHVS_NO_FILTER -> 2
        public const int MATCHVS_NO_FILTER = 2;

        /// MATCHVS_CREATE_ROOM_FAIL -> 1
        public const int MATCHVS_CREATE_ROOM_FAIL = 1;

        /// MATCHVS_JOIN_ROOM_FAIL -> 2
        public const int MATCHVS_JOIN_ROOM_FAIL = 2;

        /// MATCHVS_READY_FAIL -> 4
        public const int MATCHVS_READY_FAIL = 4;

        /// MATCHVS_START_FAIL -> 8
        public const int MATCHVS_START_FAIL = 8;
    }

    [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct matchvs_score_s {

        /// int
        public int a;

        /// int
        public int b;

        /// int
        public int c;

        /// int
        public int extend_num;

        /// int[10]
        [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst = 10, ArraySubType = System.Runtime.InteropServices.UnmanagedType.I4)]
        public int[] extend;
    }

    /// Return Type: void
    ///ret: int
    ///msg: char*
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_inited(int ret, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string msg);

    /// Return Type: void
    ///ret: int
    ///msg: char*
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_exception(int ret, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string msg);

    /// Return Type: void
    ///user_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_logined(int user_id);

    /// Return Type: void
    ///user_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_logouted(int user_id);

    /// Return Type: void
    ///room_id: int
    ///room_name: char*
    ///max_user: int
    ///could_random: int
    ///payload: void*
    ///payload_size: int
    ///first_user: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_room_created(int room_id, System.IntPtr room_name, int max_user, int could_random, System.IntPtr payload, int payload_size, int first_user);

    /// Return Type: void
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_room_released(int room_id);

    /// Return Type: void
    ///user_id: int
    ///room_id: int
    ///payload: char*
    ///payload_size: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_joined_room(int user_id, int room_id, System.IntPtr payload, int payload_size);

    /// Return Type: void
    ///user_id: int
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_leaved_room(int user_id, int room_id);

    /// Return Type: int
    ///user_id: int
    ///payload: void*
    ///payload_size: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_user_want_join_random_room(int user_id, System.IntPtr payload, int payload_size);

    /// Return Type: int
    ///user_id: int
    ///room_id: int
    ///payload: void*
    ///payload_size: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_user_want_join_room(int user_id, int room_id, System.IntPtr payload, int payload_size);

    /// Return Type: int
    ///user_id: int
    ///max_user: int
    ///could_random: int
    ///name_size: int*
    ///name_buf: char*
    ///name_buf_size: int
    ///payload: void*
    ///payload_size: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_user_want_create_room(int user_id, int max_user, int could_random, ref int name_size, System.IntPtr name_buf, int name_buf_size, System.IntPtr payload, int payload_size);

    /// Return Type: int
    ///user_id: int
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_user_want_ready_game(int user_id, int room_id);

    /// Return Type: int
    ///user_id: int
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_user_want_start_game(int user_id, int room_id);

    /// Return Type: void
    ///user_id: int
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_readyed_game(int user_id, int room_id);

    /// Return Type: void
    ///user_id: int
    ///room_id: int
    ///score: matchvs_score_t*
    ///paylod: void*
    ///payload_size: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_overed_game(int user_id, int room_id, ref matchvs_score_s score, System.IntPtr paylod, int payload_size);

    /// Return Type: void
    ///user_id: int
    ///room_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_user_cancel_readyed_game(int user_id, int room_id);

    /// Return Type: void
    ///room_id: int
    ///round_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_game_started(int room_id, int round_id);

    /// Return Type: void
    ///room_id: int
    ///round_id: int
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate void matchvs_callback_s_on_game_overed(int room_id, int round_id);

    /// Return Type: int
    ///user_id: int
    ///des_count: int
    ///users: int*
    ///event_id: int
    ///buf_size: int
    ///buf: char*
    [System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    public delegate int matchvs_callback_s_on_msg_recved(int user_id, int des_count, System.IntPtr users, int event_id, int buf_size, System.IntPtr buf);

    [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct matchvs_callback_s {

        /// matchvs_callback_s_on_inited
        public matchvs_callback_s_on_inited AnonymousMember1;

        /// matchvs_callback_s_on_exception
        public matchvs_callback_s_on_exception AnonymousMember2;

        /// matchvs_callback_s_on_user_logined
        public matchvs_callback_s_on_user_logined AnonymousMember3;

        /// matchvs_callback_s_on_user_logouted
        public matchvs_callback_s_on_user_logouted AnonymousMember4;

        /// matchvs_callback_s_on_room_created
        public matchvs_callback_s_on_room_created AnonymousMember5;

        /// matchvs_callback_s_on_room_released
        public matchvs_callback_s_on_room_released AnonymousMember6;

        /// matchvs_callback_s_on_user_joined_room
        public matchvs_callback_s_on_user_joined_room AnonymousMember7;

        /// matchvs_callback_s_on_user_leaved_room
        public matchvs_callback_s_on_user_leaved_room AnonymousMember8;

        /// matchvs_callback_s_on_user_want_join_random_room
        public matchvs_callback_s_on_user_want_join_random_room AnonymousMember9;

        /// matchvs_callback_s_on_user_want_join_room
        public matchvs_callback_s_on_user_want_join_room AnonymousMember10;

        /// matchvs_callback_s_on_user_want_create_room
        public matchvs_callback_s_on_user_want_create_room AnonymousMember11;

        /// matchvs_callback_s_on_user_want_ready_game
        public matchvs_callback_s_on_user_want_ready_game AnonymousMember12;

        /// matchvs_callback_s_on_user_want_start_game
        public matchvs_callback_s_on_user_want_start_game AnonymousMember13;

        /// matchvs_callback_s_on_user_readyed_game
        public matchvs_callback_s_on_user_readyed_game AnonymousMember14;

        /// matchvs_callback_s_on_user_overed_game
        public matchvs_callback_s_on_user_overed_game AnonymousMember15;

        /// matchvs_callback_s_on_user_cancel_readyed_game
        public matchvs_callback_s_on_user_cancel_readyed_game AnonymousMember16;

        /// matchvs_callback_s_on_game_started
        public matchvs_callback_s_on_game_started AnonymousMember17;

        /// matchvs_callback_s_on_game_overed
        public matchvs_callback_s_on_game_overed AnonymousMember18;

        /// matchvs_callback_s_on_msg_recved
        public matchvs_callback_s_on_msg_recved AnonymousMember19;
    }

    public partial class NativeMethods {

        /// Return Type: int
        ///callback: matchvs_callback_s*
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_init", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern int matchvs_init(matchvs_callback_s callback);


        /// Return Type: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvsWinHelpInit", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern int matchvsWinHelpInit();


        /// Return Type: void
        ///name_size: int
        ///name: char*
        ///max_user: int
        ///could_random: int
        ///payload: char*
        ///payload_size: int
        ///first_user: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_create_room", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_create_room(int name_size, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string name, int max_user, int could_random, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string payload, int payload_size, int first_user);


        /// Return Type: void
        ///room_id: int
        ///user_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_user_join_room", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_user_join_room(int room_id, int user_id);


        /// Return Type: void
        ///room_id: int
        ///user_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_user_leave_room", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_user_leave_room(int room_id, int user_id);


        /// Return Type: void
        ///room_id: int
        ///user_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_user_ready_game", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_user_ready_game(int room_id, int user_id);


        /// Return Type: void
        ///room_id: int
        ///user_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_user_cancel_ready_game", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_user_cancel_ready_game(int room_id, int user_id);


        /// Return Type: void
        ///room_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_start_game", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_start_game(int room_id);


        /// Return Type: void
        ///room_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_over_game", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_over_game(int room_id);


        /// Return Type: void
        ///room_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_close_room_in", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_close_room_in(int room_id);


        /// Return Type: void
        ///room_id: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_open_room_in", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_open_room_in(int room_id);


        /// Return Type: void
        ///room_id: int
        ///num: int
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_set_room_max", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_set_room_max(int room_id, int num);


        /// Return Type: void
        ///user_id: int
        ///opt: int
        ///error: char*
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_opt_fail", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_opt_fail(int user_id, int opt, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string error);


        /// Return Type: void
        ///room_id: int
        ///src_id: int
        ///des_count: int
        ///users: int*
        ///event_id: int
        ///buf_size: int
        ///buf: char*
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_send_msg", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_send_msg(int room_id, int src_id, int des_count, ref int users, int event_id, int buf_size, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string buf);


        /// Return Type: void
        ///room_id: int
        ///user_id: int
        ///score: matchvs_score_t*
        [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "matchvs_send_game_score", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void matchvs_send_game_score(int room_id, int user_id, ref matchvs_score_s score);

    }

}