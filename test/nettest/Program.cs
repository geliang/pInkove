﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;

namespace nettest
{

    class Program
    {

        public static string ptrToString(System.IntPtr ptr, int size) {
            byte[] managedArray = new byte[size];
            Marshal.Copy(ptr, managedArray, 0, size);
            return Encoding.Default.GetString(managedArray);
        }
        public static byte[] ptrToByteArray(System.IntPtr ptr, int size) {
            byte[] managedArray = new byte[size];
            Marshal.Copy(ptr, managedArray, 0, size);
            return managedArray;
        }
        public static int[] ptrToIntArray(System.IntPtr ptr, int size) {
            int[] temp = new int[4];
            for(int i = 0; i < 4; i++) {
                var p = (int)Marshal.PtrToStructure(
                    ptr + i * Marshal.SizeOf(typeof(int)),
                    typeof(int));
                temp[i] = p;
            }
            return temp;
        }


        public static void CallbackIntCallback(int a)
        {
            Console.WriteLine("callback int: {0}", a);
        }

        public static void CallbackHeapCallback(string str)
        {
            Console.WriteLine("callback heap: {0}", str);
        }

        public static void CallbackStackCallback(string str)
        {
            test(str);
           
            Console.WriteLine("callback stack: {0}", str);
           
        }
       
        public static void CallbackMultImp(String msg, int a, float b,  IntPtr ptr) {
            //test(msg);
            Console.WriteLine("long is :"+ Marshal.SizeOf(typeof(Int64)));

            int[] temp = new int[4];
            for(int i = 0; i < 4; i++) {
                var p = (int)Marshal.PtrToStructure(
                    ptr + i * Marshal.SizeOf(typeof(int)),
                    typeof(int));
                temp[i] = p;
            }

            Console.WriteLine(" CallbackMultImp: {0} {1} {2} {3}", msg,""+a,""+b,""+ temp[2]);

        }

        
        public static void matchvs_callback_s_on_exception_imp(int ret,  string msg) {
            Console.WriteLine(" matchvs_callback_s_on_exception_imp: "+ ret+ "" + msg );
        }
        public static int matchvs_callback_s_on_msg_recved_imp(int user_id, int des_count, System.IntPtr users, int event_id, int buf_size, System.IntPtr buf) {
            Console.WriteLine(" matchvs_callback_s_on_msg_recved_imp: " + ptrToIntArray(users,des_count)[0]+ " " + ptrToString (buf, buf_size));
            return 0;
        }
        public static int test(string s) {
            Console.WriteLine("test : {0}", s);
            return 0;
        }
        static callback_s callback;
        static MatchVS.matchvs_callback_s mcs;


        public static void matchvs_callback_s_on_inited(int ret,  string msg) {
            Console.WriteLine("ret : {0}", msg  + " ret "+ ret);
        }

        static void Main(string[] args)
        {

            Console.WriteLine("======start================");
            // CallbackIntDelegate mytest = new CallbackIntDelegate(ShowIntCallBack);

            //setCallbackInt(mCallbackInt);

            //setCallbackHeap(mCallbackHeap);
            //setCallbackStack(mCallbackStack);

            callback.AnonymousMember4 = CallbackMultImp;

            NativeMethods.setCallback(callback);
            GC.Collect();
            new Thread(()=> {
                Thread.Sleep(100);
                GC.Collect();
            }) .Start();

            MatchVS.NativeMethods.matchvsWinHelpInit();
            mcs.AnonymousMember1 = matchvs_callback_s_on_inited;
            mcs.AnonymousMember2 = matchvs_callback_s_on_exception_imp;
            mcs.AnonymousMember19 = matchvs_callback_s_on_msg_recved_imp;
            MatchVS.NativeMethods.matchvs_init(mcs);
            string name = "天子一号";
            string payload  ="payload zhong问";
            Console.WriteLine("Encoding:" + Encoding.Default.EncodingName);
            MatchVS.NativeMethods.matchvs_create_room(Encoding.Default.GetByteCount(name), name,10, 888, payload, payload.Length,42);
            int[] array = new int[] { 1, 2, 3 };


            MatchVS.NativeMethods.matchvs_send_msg(0,0,0, ref array[0], 0,3,"abc");
            NativeMethods.init();
            Console.WriteLine("======end================");
        }

    }
}
