﻿
public partial class NativeConstants {

    /// _TEST_H_ -> 
    /// Error generating expression: 值不能为空。
    ///参数名: node
    public const string _TEST_H_ = "";

    /// DLLAPI -> __declspec(dllexport)
    /// Error generating expression: Error generating function call.  Operation not implemented
    public const string DLLAPI = "__declspec(dllexport)";

    /// FunStackPushType -> __stdcall
    /// Error generating expression: Value __stdcall is not resolved
    public const string FunStackPushType = "__stdcall";
}

/// Return Type: void
///ret: int
[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
public delegate void callback_s_callbackInt(int ret);

/// Return Type: void
///msg: char*
[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
public delegate void callback_s_callbackHeap([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string msg);

/// Return Type: void
///msg: char*
[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
public delegate void callback_s_callbackStack([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string msg);

/// Return Type: void
///msg: char*
///a: int
///b: int
///arraya: int*
[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
public delegate void callback_s_callbackMult([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string msg, int a, float b, System.IntPtr arraya);

/// Return Type: void
///size: int*
///arraya: char*
[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
public delegate void callback_s_callbackIntptr(ref int size, System.IntPtr arraya);

[System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
public struct callback_s {

    /// callback_s_callbackInt
    public callback_s_callbackInt AnonymousMember1;

    /// callback_s_callbackHeap
    public callback_s_callbackHeap AnonymousMember2;

    /// callback_s_callbackStack
    public callback_s_callbackStack AnonymousMember3;

    /// callback_s_callbackMult
    public callback_s_callbackMult AnonymousMember4;

    /// callback_s_callbackIntptr
    public callback_s_callbackIntptr AnonymousMember5;
}

public partial class NativeMethods {

    /// Return Type: void
    ///cb: callback_s
    [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "setCallback", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
    public static extern void setCallback(callback_s cb);


    /// Return Type: void
    [System.Runtime.InteropServices.DllImportAttribute("test", EntryPoint = "init", CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
    public static extern void init();


}
