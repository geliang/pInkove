#ifndef __MATCHVS_H__
#define __MATCHVS_H__

#if defined(WIN32)  || defined(__WIN32__) || defined(_WIN32) || defined(_WIN64) || defined(__WINDOWS__) 
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI  
#endif

#ifdef __cplusplus
extern "C" {
    #endif

    /** @brief 已经处理 */
    #define MATCHVS_DEALING              0
    /** @brief 失败 */
    #define MATCHVS_FAILED               1
    /** @brief 不过滤 */
    #define MATCHVS_NO_FILTER            2

    /** @brief 创建房间失败 */
    #define MATCHVS_CREATE_ROOM_FAIL		1
    /** @brief 加入房间失败 */
    #define MATCHVS_JOIN_ROOM_FAIL			2
    /** @brief 准备失败 */
    #define MATCHVS_READY_FAIL				4
    /** @brief 开始失败 */
    #define MATCHVS_START_FAIL				8


    #pragma pack(1)
    typedef struct matchvs_score_s matchvs_score_t;

    /** @brief 用户分数 */
    struct matchvs_score_s
    {
        /** 分数字段a */
        int 						a;
        /** 分数字段b */
        int 						b;
        /** 分数字段c */
        int 						c;
        /** 分数拓展字段个数 */
        int                         extend_num;
        /** 分数拓展字段列表（个数由extend_num决定） */
        int                         extend[10];
    };

    typedef struct matchvs_callback_s    matchvs_callback_t;

    /** @brief 游戏CP回调接口集合 */
    struct matchvs_callback_s
    {
        /** @brief 初始化回调 */
        void(*on_inited)(int ret, const char *msg);
        /** @brief 异常回掉 */
        void(*on_exception)(int ret, const char *msg);

        /** @brief 用户登录回调 */
        void(*on_user_logined)(int user_id);
        /** @brief 用户登出回调 */
        void(*on_user_logouted)(int user_id);
        /** @brief 创建房间回调 */
        void(*on_room_created)(int room_id, char *room_name, int max_user, int could_random, void *payload, int payload_size, int first_user);
        /** @brief 销毁房间回调 */
        void(*on_room_released)(int room_id);
        /** @brief 用户加入房间回调 */
        void(*on_user_joined_room)(int user_id, int room_id, char *payload, int payload_size);
        /** @brief 用户离开房间回调 */
        void(*on_user_leaved_room)(int user_id, int room_id);
        /** @brief 用户请求加入随机房间回调 */
        int(*on_user_want_join_random_room)(int user_id, void *payload, int payload_size);
        /** @brief 用户请求加入指定房间回调 */
        int(*on_user_want_join_room)(int user_id, int room_id, void *payload, int payload_size);
        /** @brief 用户请求创建房间回调 */
        int(*on_user_want_create_room)(int user_id, int max_user, int could_random, int *name_size, char *name_buf, int name_buf_size, void *payload, int payload_size);
        /** @brief 用户请求游戏准备回调 */
        int(*on_user_want_ready_game)(int user_id, int room_id);
        /** @brief 用户请求游戏开始回调 */
        int(*on_user_want_start_game)(int user_id, int room_id);
        /** @brief 用户准备游戏回调 */
        void(*on_user_readyed_game)(int user_id, int room_id);
        /** @brief 用户游戏结束回调 */
        void(*on_user_overed_game)(int user_id, int room_id, matchvs_score_t *score, void* paylod, int payload_size);
        /** @brief 用户取消准备游戏回调 */
        void(*on_user_cancel_readyed_game)(int user_id, int room_id);
        /** @brief 游戏开始通知下发（通知所有用户） */
        void(*on_game_started)(int room_id, int round_id);
        /** @brief 游戏结束通知下发（通知所有用户） */
        void(*on_game_overed)(int room_id, int round_id);
        /** @brief 接收到用户数据中转 （抓发给其他用户） */
        int(*on_msg_recved)(int user_id, int des_count, int *users, int event_id, int buf_size, char *buf);
    };
    #pragma pack()

    /** @brief 初始化 */
    DLLAPI int        matchvs_init(matchvs_callback_t *callback);

    /** @brief 初始化命名管道针对C#使用，其他语言不需要调用 */
    DLLAPI int        matchvsWinHelpInit();


    /** @brief 创建房间（后台主动） */
    DLLAPI void       matchvs_create_room(int name_size, char *name, int max_user, int could_random, void *payload, int payload_size, int first_user);
    /** @brief 将用户加入到房间（后台主动） */
    DLLAPI void       matchvs_user_join_room(int room_id, int user_id);
    /** @brief 将用户踢出房间（后台主动） */
    DLLAPI void       matchvs_user_leave_room(int room_id, int user_id);
    /** @brief 设置用户游戏准备（后台主动） */
    DLLAPI void       matchvs_user_ready_game(int room_id, int user_id);
    /** @brief 设置用户游戏取消准备（后台主动） */
    DLLAPI void       matchvs_user_cancel_ready_game(int room_id, int user_id);
    /** @brief 设置用户游戏开始（后台主动） */
    DLLAPI void       matchvs_start_game(int room_id);
    /** @brief 结束指定房间游戏局（后台主动） */
    DLLAPI void       matchvs_over_game(int room_id);
    /** @brief 关闭加入指定房间开关（后台主动） */
    DLLAPI void       matchvs_close_room_in(int room_id);
    /** @brief 打开加入指定房间开关（后台主动） */
    DLLAPI void       matchvs_open_room_in(int room_id);
    /** @brief 设置房间最大用户个数（后台主动） */
    DLLAPI void       matchvs_set_room_max(int room_id, int num);
    /** @brief 操作失败通知（后台主动） */
    DLLAPI void       matchvs_opt_fail(int user_id, int opt, const char* error);
    /** @brief 发送通知消息（后台主动） */
    DLLAPI void       matchvs_send_msg(int room_id, int src_id, int des_count, int *users, int  event_id, int buf_size, char *buf);
    /** @brief 指定用户分数下发（后台主动） */
    DLLAPI void       matchvs_send_game_score(int room_id, int user_id, matchvs_score_t *score);

    #ifdef __cplusplus
}
#endif


#endif
