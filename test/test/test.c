﻿#include "test.h"
#include <stdio.h>
#include <windows.h>

#include "matchvs.h"
matchvs_callback_s mcallback ;

callback_s		g_cb;

DWORD WINAPI ThreadFun(LPVOID pM)
{
    int i = 0;
    int arrayi[4] = { 1,2,3,4 };
	while (1)
	{
		printf("thread loop\n");
        i++;
		int abc = 100;
		char buffer[1024];
		char *pstack = 0;
		memset(buffer, 0, 1024);
		memcpy(buffer, "heap", strlen("heap"));

		pstack = malloc(16);
		memset(pstack, 0, 16);
		memcpy(pstack, "stack", strlen("stack"));

		//callbackptr->callbackInt(abc);
		//callbackptr->callbackHeap(buffer);
		//callbackptr->callbackStack(pstack);
		if(g_cb.callbackMult)
		{
            printf("cbMult ++++++++\n");
            Sleep(3000);
            char outs[1024];
            printf("sprintf_s \n ");
            sprintf_s(outs, 1024,"'中文 mult ,int64_t is %d'", sizeof(long long));
            printf("cbMult %s",outs);
            g_cb.callbackMult(outs, i, i, arrayi);
            printf(" 1 \n");
		}
		else
		{
			printf("cb int is null\n");
		}
        if (mcallback.on_exception)
        {
            mcallback.on_exception(0, "not imp all interface");
        }
        if (mcallback.on_msg_recved)
        {
            int  ar[] = {1,2,3};
            mcallback.on_msg_recved(1,3,ar,0,4,"xxxx");
        }
        printf(" 1 \n");
		free(pstack);
        printf(" 2 \n");
		Sleep(1000);
        printf(" 3 \n");
	}

	

	return 0;
}

void  FunStackPushType init()
{

	//callbackptr = callback;
	int i = 0;

	HANDLE handle = CreateThread(NULL, 0, ThreadFun, NULL, 0, NULL);
	printf("-----------init--------------\n");

	while (i++ < 10)
	{
        Sleep(2000);
	}

	WaitForSingleObject(handle, INFINITE);
	return 0;

}




void  FunStackPushType setCallback(callback_s cb)
{
    g_cb = cb;
}

int  FunStackPushType matchvs_init(matchvs_callback_s cb){
    printf("-----------matchvs_init--------------\n");
    printf("-----------matchvs_init--------------\n");
    mcallback = cb;
    cb.on_inited(100, "OK");
    printf("-----------matchvs_init--------------\n");
    printf("-----------matchvs_init--------------\n");
    printf("-----------matchvs_init--------------\n");
}
int FunStackPushType matchvsWinHelpInit(){
    printf("-----------matchvsWinHelpInit--------------\n");
    printf("-----------matchvsWinHelpInit--------------\n");
    printf("-----------matchvsWinHelpInit--------------\n");
    printf("-----------matchvsWinHelpInit--------------\n");
    printf("-----------matchvsWinHelpInit--------------\n");
}

DLLAPI void  FunStackPushType   matchvs_create_room(int name_size, char *name, int max_user, 
                          int could_random, void *payload, int payload_size, int first_user){
    printf("-----------matchvs_create_room--------------\n");
    printf("- - name_size %d name %s  (天子二号 %d sizeof %d)\n " , name_size , name,strlen("天子二号"),sizeof("天子二号"));
    printf("- - payload %s\n" , (char*)payload);
}

DLLAPI void   FunStackPushType    matchvs_send_msg(int room_id, int src_id, int des_count, int *users, int  event_id, int buf_size,const  char *buf){
    printf("-----------matchvs_send_msg--------------\n");
    printf("----------- users %d %d %d  %s--------------\n",users[0],users[1],users[2],buf);
}