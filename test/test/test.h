#ifndef _TEST_H_
#define _TEST_H_

#define DLLAPI __declspec(dllexport)
#define FunStackPushType __stdcall
//#define FunStackPushType 

#ifdef __cplusplus
extern "C" {
#endif
#pragma pack(1)
typedef struct callback_s
{
    void(FunStackPushType *callbackInt)(int ret);
    void(FunStackPushType *callbackHeap)(const char *msg);
    void(FunStackPushType *callbackStack)(const char *msg);
    void(FunStackPushType *callbackMult)(const char *msg, int a, float b, int * arraya);
    void(FunStackPushType *callbackIntptr)(int* size, char *arraya);
}callback_s;

DLLAPI void  FunStackPushType setCallback(callback_s cb);
DLLAPI void  FunStackPushType init();

#pragma pack()
#ifdef __cplusplus
}
#endif

#endif


