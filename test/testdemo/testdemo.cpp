// testdemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <test.h>

static void callbackInt(int ret);
static void callbackHeap(const char *msg);
static void callbackStack(const char *msg);


void callbackInt(int ret)
{
	printf("-int:%d-\n", ret);
}

void callbackHeap(const char *msg)
{
	printf("-heap:%s-\n", msg);
}
void callbackStack(const char *msg)
{
	printf("-stack:%s-\n", msg);
}

callback_t cb = {
	callbackInt,
	callbackHeap,
	callbackStack
};

int main()
{

	init(&cb);
    return 0;
}

